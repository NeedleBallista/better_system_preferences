import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPasswordField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
public class appletWindow extends JFrame{
	private preferenceFunctions rabbit;
	private JPasswordField password;
	
	public appletWindow(){
		super("Created by NeedleBallista");
		setLayout (new FlowLayout());
		
		password = new JPasswordField("mypassword");
		add(password);
		rabbit = new preferenceFunctions();
		theHandler handler = new theHandler();
		password.addActionListener(handler);
	}
	
	private class theHandler implements ActionListener{
		String string = ("we done here");
		public void actionPerformed(ActionEvent event){
			if(event.getSource() == password)
				rabbit.setNewPass(event.getActionCommand());
			JOptionPane.showMessageDialog(null, string);

		}
	}

}

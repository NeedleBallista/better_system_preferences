import java.io.IOException;


public class preferenceFunctions {
	private static String newPass;
	private static String username;
	
	public preferenceFunctions(){
		username = System.getProperty("user.name");
	}
	
	public void setNewPass(String newP){
		newPass = newP;
		}

static void toggleLock()
  {
    Runtime runtime = Runtime.getRuntime();
   
    String[] args = { "osascript", "-e", "tell application \"System Events\" to set security preferences's require password to wake to not (security preferences's require password to wake) "};
    try
   
    {
      Process process = runtime.exec(args);
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }
static void setPassword()
	{
	Runtime runtime = Runtime.getRuntime();
	
	String[] args = { "osascript", "-e", "do shell script \"dscl . -passwd /Users/"+ username + " "+newPass+"\""};
	try
	   
    {
      Process process = runtime.exec(args);
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
	}
static void setClock(){
	
	Runtime runtime = Runtime.getRuntime();
	
	String[] args = { "osascript", "-e", "do shell script \"defaults write com.apple.MenuBarClock ClockEnabled -bool false\""};
	
	try
	   
    {
      Process process = runtime.exec(args);
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
}
	static void applySettings(){
		
		Runtime runtime = Runtime.getRuntime();
		
		String[] args = { "osascript", "-e", "do shell script \"killall SystemUIServer\""};
		
		try
		   
	    {
	      Process process = runtime.exec(args);
	    }
	    catch (IOException e)
	    {
	      e.printStackTrace();
	    }
	}
static void showFiles()
	{
	Runtime runtime = Runtime.getRuntime();
	
	String[] args = { "osascript", "-e", "do shell script \"defaults write com.apple.finder AppleShowAllFiles -bool true\""};
	try
	   
    {
      Process process = runtime.exec(args);
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
	}
		
	}
